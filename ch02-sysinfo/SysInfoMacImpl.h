#ifndef SYSINFOMACIMPL_H
#define SYSINFOMACIMPL_H

#include <QtGlobal>
#include <QVector>

#include "SysInfo.h"

class SysInfoMacImpl : public SysInfo
{
public:
    SysInfoMacImpl();

    // SysInfo interface
public:
    void init() override;
    double cpuLoadAverage() override;
    double memoryUsed() override;

private:
    QVector<qulonglong> cpuRawData();
    QVector<qulonglong> mCpuLoadLastValues;
};

#endif // SYSINFOMACIMPL_H
