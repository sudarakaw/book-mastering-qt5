QT       += core gui charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++14

# Name of the executable (default to .pro filename)
TARGET = sysinfo

# Type of the binary generated (app = exeutable, lib = library)
TEMPLATE = app

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    CpuWidget.cpp \
    MemoryWidget.cpp \
    SysInfo.cpp \
    SysInfoWidget.cpp \
    main.cpp \
    MainWindow.cpp

HEADERS += \
    CpuWidget.h \
    MainWindow.h \
    MemoryWidget.h \
    SysInfo.h \
    SysInfoWidget.h

# debug scope can be nested in platform
#
# debug {
# }
#
# linux {
#   debug {
#   }
# }

# scopes can be combined and/or use like if-else statements
#
# linux | win {
# } else:macx {
# } else {
# }

windows {
    SOURCES += \
        SysInfoWindowsImpl.cpp

    HEADERS += \
        SysInfoWindowsImpl.h
}

linux {
    SOURCES += \
        SysInfoLinuxImpl.cpp

    HEADERS += \
        SysInfoLinuxImpl.h
}

macx {
    SOURCES += \
        SysInfoMacImpl.cpp

    HEADERS += \
        SysInfoMacImpl.h
}

FORMS += \
    MainWindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
