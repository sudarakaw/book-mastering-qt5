#ifndef TESTTRACK_H
#define TESTTRACK_H

#include <QObject>

class TestTrack : public QObject
{
    Q_OBJECT
public:
    explicit TestTrack(QObject *parent = nullptr);

private slots:
    void addSoundEvent_data();
    void addSoundEvent();
};

#endif // TESTTRACK_H
