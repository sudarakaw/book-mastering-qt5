import Qt3D.Core 2.12
import Qt3D.Render 2.12
import Qt3D.Extras 2.12

Entity {
    id: sceneRoot

    RenderSettings {
        id: renderSettings

        activeFrameGraph: ForwardRenderer {
            clearColor: Qt.rgba(0, 0, 0, 1)
        }
    }

    Apple {
        id: apple
        position: Qt.vector3d(3.0, 0.0, 2.0)
    }

    components: [frameGraph]
}
