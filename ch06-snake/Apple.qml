import Qt3D.Core 2.12
import Qt3D.Render 2.12
import Qt3D.Extras 2.12

import "factory.js" as Factory

GameEntity {
    id: root
    type: Factory.APPLE_TYPE

    property alias position: transform.translation
    property alias color: material.diffuse

    DiffuseMapMaterial {
        id: material
        diffuse: TextureLoader {
            source: "qrc:/models/apple-texture.png"
        }
    }

    Mesh {
        id: mesh
        source: "models/apple.obj"
    }

    Transform {
        id: transform
        scale: 0.5
    }

    components: [material, mesh, transform]
}
