QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++14
TARGET = ch09-mandelbrot
TEMPLATE = app

# Fix for c++14 compatibility issue on Arch
QMAKE_CXXFLAGS += "-fno-sized-deallocation"

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Job.cpp \
    MandelbrotCalculator.cpp \
    MandelbrotWidget.cpp \
    main.cpp \
    MainWindow.cpp

HEADERS += \
    Job.h \
    JobResult.h \
    MainWindow.h \
    MandelbrotCalculator.h \
    MandelbrotWidget.h

FORMS += \
    MainWindow.ui

