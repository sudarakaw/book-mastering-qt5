QT += core widgets

TARGET = $$qtLibraryTarget(filter-plugin-blur)
TEMPLATE = lib

CONFIG += plugin

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    FilterBlur.cpp

HEADERS += \
    FilterBlur.h

include(../plugins-common.pri)
include(../plugins-common-opencv.pri)
