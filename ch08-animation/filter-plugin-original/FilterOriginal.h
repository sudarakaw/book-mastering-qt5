#ifndef FILTERORIGINAL_H
#define FILTERORIGINAL_H

#include <QObject>

#include <Filter.h>

class FilterOriginal : public QObject, Filter
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "dev.suda.masteringqt5.animation.filters.Filter.v1")
    Q_INTERFACES(Filter)

public:
    FilterOriginal(QObject* parent = 0);
    ~FilterOriginal();

    QString name() const override;
    QImage process(const QImage& image) override;
};

#endif // FILTERORIGINAL_H
