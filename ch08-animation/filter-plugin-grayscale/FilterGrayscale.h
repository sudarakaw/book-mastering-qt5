#ifndef FILTERGRAYSCALE_H
#define FILTERGRAYSCALE_H

#include <QObject>

#include <Filter.h>

class FilterGrayscale : public QObject, Filter
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "dev.suda.masteringqt5.animation.filters.Filter.v1")
    Q_INTERFACES(Filter)

public:
    FilterGrayscale(QObject* parent = 0);
    ~FilterGrayscale();

    QString name() const override;
    QImage process(const QImage& image) override;
};

#endif // FILTERGRAYSCALE_H
