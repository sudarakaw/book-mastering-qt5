#include <QDBusInterface>
#include <QDBusReply>
#include <QDebug>

int main(int /*argc*/, char */*argv*/[])
{
    QString serviceName("dev.suda.qt5.hello");

    QDBusInterface serviceInterface(serviceName, "/");
    QDBusReply<QString> response = serviceInterface.call(
                "say", "QT5 Tester");

    qDebug().noquote() << response;
}
