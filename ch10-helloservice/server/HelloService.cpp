#include "HelloService.h"

#include <QDebug>

QString HelloService::say(const QString &name)
{
    qDebug().noquote() << name << " is here!";

    return QString("Hello %1!").arg(name);
}
