#include "HelloService.h"

#include <QtDBus/QDBusConnection>

int main(int /*argc*/, char* /*argv*/[])
{
    HelloService helloService;
    QString serviceName("dev.suda.qt5.hello");

    QDBusConnection::sessionBus().registerService(serviceName);
    QDBusConnection::sessionBus()
            .registerObject("/",
                            &helloService,
                            QDBusConnection::ExportAllSlots);
    while (1) {}
}
