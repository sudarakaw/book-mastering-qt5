#ifndef HELLOSERVICE_H
#define HELLOSERVICE_H

#include <QObject>

class HelloService : public QObject
{
    Q_OBJECT
public slots:
    QString say(const QString &name);
};

#endif // HELLOSERVICE_H
