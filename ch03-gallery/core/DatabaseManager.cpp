#include "DatabaseManager.h"

#include <QSqlDatabase>

DatabaseManager &DatabaseManager::instance()
{
#if defined (Q_OS_ANDROID) || defined (Q_OS_IOS)
    QFile assetDbFile(":/database/" + DATABASE_FILENAME);
    QString destinationDbFile = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation)
            .append("/" + DATABASE_FILENAME);

    qDebug() << "Assets file " << assetDbFile.filename();
    qDebug() << "Asset destination " << destinationDbFile;

    if(!QFile::exists(destinationDbFile)) {
        assetDbFile.copy(destinationDbFile);
        QFile::setPermissions(destinationDbFile, QFile::WriteOwner | QFile::ReadOwner);

        qDebug() << "Setted permissionson copied file";
    }

    static DatabaseManager singleton(destinationDbFile);
#else
    static DatabaseManager singleton;
#endif

    return singleton;
}

DatabaseManager::~DatabaseManager()
{
   mDatabase->close();
}

DatabaseManager::DatabaseManager(const QString& path) :
    mDatabase(new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE"))),
    albumDao(*mDatabase),
    pictureDao(*mDatabase)
{
    mDatabase->setDatabaseName(path);
    mDatabase->open();

    albumDao.init();
    pictureDao.init();
}
