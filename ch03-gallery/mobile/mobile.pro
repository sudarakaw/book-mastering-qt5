QT += qml quick sql svg

TEMPLATE = app
TARGET = gallery-mobile

CONFIG += c++14

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        PictureImageProvider.cpp \
        main.cpp

RESOURCES += gallery.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../core/release/ -lgallery
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../core/debug/ -lgallery
else:unix: LIBS += -L$$OUT_PWD/../core/ -lgallery

INCLUDEPATH += $$PWD/../core
DEPENDPATH += $$PWD/../core

contains(ANDROID_TARGET_ARCH,x86) {
    ANDROID_EXTRA_LIBS = $$[QT_INSTALL_LIBS]/libQt5Sql.so
}

HEADERS += \
    PictureImageProvider.h
