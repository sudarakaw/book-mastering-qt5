#ifndef WORKERWIDGET_H
#define WORKERWIDGET_H

#include "Worker.h"

#include <QProgressBar>
#include <QThread>
#include <QTimer>
#include <QWidget>

class WorkerWidget : public QWidget
{
    Q_OBJECT
public:
    explicit WorkerWidget(QWidget *parent = nullptr);
    ~WorkerWidget();

private:
    QProgressBar mStatus;
    Worker mWorker;
    QThread mWorkerThread;
    QTimer mRefreshTimer;
};

#endif // WORKERWIDGET_H
