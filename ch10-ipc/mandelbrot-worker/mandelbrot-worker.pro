QT += core gui widgets network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++14

# Fix for c++14 compatibility issue on Arch
QMAKE_CXXFLAGS += "-fno-sized-deallocation"

INCLUDEPATH += $$PWD/../sdk
DEPENDPATH += $$PWD/../sdk

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Job.cpp \
    Worker.cpp \
    WorkerWidget.cpp \
    main.cpp

HEADERS += \
    Job.h \
    Worker.h \
    WorkerWidget.h

