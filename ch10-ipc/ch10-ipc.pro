TEMPLATE = subdirs

OTHER_FILES += \
    sdk/Message.h \
    sdk/JobRequest.h \
    sdk/MessageUtils.h \
    sdk/JobResult.h

SUBDIRS += \
    mandelbrot-app \
    mandelbrot-worker
