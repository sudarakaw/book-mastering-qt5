# Mastering Qt 5 - Code Samples

Practice code written while following "Mastering Qt 5" book.

- Book [Mastering Qt 5 - Second Edition](https://www.packtpub.com/product/mastering-qt-5-second-edition/9781788995399) by [Guillaume Lazar](https://github.com/GuillaumeLazar) and [Robin Penea](https://twitter.com/synapticrob)

