#include "PluginDesigner.h"
#include "FilterWidget.h"

PluginDesigner::PluginDesigner(QObject *parent) :
    QObject(parent),
    mInitialized(false)
{
}

QString PluginDesigner::name() const
{
    return "FilterWidget";
}

QString PluginDesigner::group() const
{
    return "Mastering Qt5";
}

QString PluginDesigner::toolTip() const
{
    return "A filtered picture";
}

QString PluginDesigner::whatsThis() const
{
    return "The filter widget applied an image effect";
}

QString PluginDesigner::includeFile() const
{
    return "FilterWidget.h";
}

QIcon PluginDesigner::icon() const
{
    return QIcon(":/icon.jpg");
}

bool PluginDesigner::isContainer() const
{
    return false;
}

QWidget *PluginDesigner::createWidget(QWidget *parent)
{
    return new FilterWidget(parent);
}

bool PluginDesigner::isInitialized() const
{
    return mInitialized;
}

void PluginDesigner::initialize(QDesignerFormEditorInterface* /*core*/)
{
   if(mInitialized)
       return;

   mInitialized = true;
}
