#ifndef FILTERWIDGET_H
#define FILTERWIDGET_H

#include <QWidget>

#include "Filter.h"
#include "plugin-designer_global.h"

namespace Ui {
class FilterWidget;
}

class PLUGINDESIGNERSHARED_EXPORT FilterWidget : public QWidget
{
    Q_OBJECT

public:
    enum FilterType { Original, Blur, Grayscale };
    Q_ENUM(FilterType)

    Q_PROPERTY(QString title READ title WRITE setTitle)
    Q_PROPERTY(FilterType filterType READ filterType WRITE setFilterType)

    explicit FilterWidget(QWidget *parent = nullptr);
    ~FilterWidget();

    void process();
    void setSourcePicture(const QImage& sourcePicture);
    void updateThumbnail(const QImage& sourceThumbnail);
    QString title() const;
    FilterType filterType() const;

public slots:
    void setTitle(const QString& title);
    void setFilterType(FilterType filterType);

signals:
    void pictureProcessed(const QImage& picture);

protected:
    void mousePressEvent(QMouseEvent*) override;

private:
    Ui::FilterWidget *ui;
    std::unique_ptr<Filter> mFilter;
    FilterType mFilterType;

    QImage mDefaultSourcePicture;
    QImage mSourcePicture;
    QImage mSourceThumbnail;

    QImage mFilteredPicture;
    QImage mFilteredThumbnail;
};

#endif // FILTERWIDGET_H
