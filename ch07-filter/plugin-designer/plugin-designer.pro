QT += widgets uiplugin

CONFIG += plugin
CONFIG += c++14

TEMPLATE = lib

DEFINES += FILTERPLUGINDESIGNER_LIBRARY

TARGET = $$qtLibraryTarget(filter-$$TARGET)
INSTALLS += target

windows {
    target.path = $$(QTDIR)/../../Tools/QtCreator/bin/plugins/designer

    debug:target_lib.files = $$OUT_PWD/debug/$${TARGET}.lib
    release:target_lib.files = $$OUT_PWD/release/$${TARGET}.lib
    target_lib.path = $$(QTDIR)/../../Tools/QtCreator/bin/plugins/designer
    INSTALLS += target_lib

    INCLUDEPATH += $$(OPENCV_HOME)/../../include
    LIBS += -L$$(OPENCV_HOME)/lib
        -lopencv_core320
        -lopencv_imgproc320
}

linux {
    target.path = $$(QTDIR)/../../Tools/QtCreator/lib/Qt/plugins/designer/
    CONFIG += link_pkgconfig
    PKGCONFIG += opencv4
}

macx {
    target.path = "$$(QTDIR)/../../QtCreator.app/Contents/PlugIns/designer/"
    target_lib.files = $$OUT_PWD/lib$${TARGET}.dylib
    target_lib.path = "$$(QTDIR)/../../QtCreator.app/Contents/PlugIns/designer/"
    INSTALLS += target_lib

    INCLUDEPATH += /usr/local/Cellar/opencv/3.2.0/include/

    LIBS += -L/usr/local/lib \
         -lopencv_core \
         -lopencv_imgproc
}

HEADERS += \
    Filter.h \
    FilterBlur.h \
    FilterGrayscale.h \
    FilterOriginal.h \
    FilterWidget.h \
    PluginDesigner.h \
    plugin-designer_global.h

SOURCES += \
    Filter.cpp \
    FilterBlur.cpp \
    FilterGrayscale.cpp \
    FilterOriginal.cpp \
    FilterWidget.cpp \
    PluginDesigner.cpp

FORMS += \
    FilterWidget.ui

RESOURCES += \
    plugin-designer.qrc

DISTFILES +=
