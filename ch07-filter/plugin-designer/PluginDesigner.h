#ifndef PLUGINDESIGNER_H
#define PLUGINDESIGNER_H

#include <QDesignerCustomWidgetInterface>

class PluginDesigner : public QObject, public QDesignerCustomWidgetInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "dev.suda.imagefilter.FilterWidgetPluginInterface")
    Q_INTERFACES(QDesignerCustomWidgetInterface)
public:
    PluginDesigner(QObject* parent = 0);
    QString name() const override;
    QString group() const override;
    QString toolTip() const override;
    QString whatsThis() const override;
    QString includeFile() const override;
    QIcon icon() const override;
    bool isContainer() const override;
    QWidget* createWidget(QWidget* parent) override;
    bool isInitialized() const override;
    void initialize(QDesignerFormEditorInterface* core) override;

private:
    bool mInitialized;
};

#endif // PLUGINDESIGNER_H
