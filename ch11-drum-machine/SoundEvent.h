#ifndef SOUNDEVENT_H
#define SOUNDEVENT_H

#include "Serializable.h"

#include <QtGlobal>

class SoundEvent : public Serializable
{
public:
    SoundEvent(qint64 timestamp = 0, int soundId = 0);
    ~SoundEvent();

    qint64 timestamp;
    int soundId;

    // Serializable interface
public:
    QVariant toVariant() const override;
    void fromVariant(const QVariant &variant) override;
};

#endif // SOUNDEVENT_H
