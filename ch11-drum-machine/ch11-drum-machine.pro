QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++14

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += MainWindow.cpp \
    SoundEffectWidget.cpp \
    Track.cpp \
    PlaybackWorker.cpp \
    SoundEvent.cpp \
    JsonSerializer.cpp \
    XmlSerializer.cpp \
    BinarySerializer.cpp \
    main.cpp

HEADERS  += MainWindow.h \
    SoundEffectWidget.h \
    Track.h \
    SoundEvent.h \
    PlaybackWorker.h \
    Serializable.h \
    Serializer.h \
    JsonSerializer.h \
    XmlSerializer.h \
    BinarySerializer.h

FORMS    += MainWindow.ui

RESOURCES += res/resource.qrc
