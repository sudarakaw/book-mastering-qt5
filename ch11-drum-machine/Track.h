#ifndef TRACK_H
#define TRACK_H

#include "SoundEvent.h"

#include <memory>
#include <vector>
#include <QElapsedTimer>
#include <QObject>
#include <QVector>

class Track : public QObject, public Serializable
{
    Q_OBJECT
public:
    enum class State {
        STOPPED,
        PLAYING,
        RECORDING,
    };

    explicit Track(QObject *parent = nullptr);
    ~Track();

    qint64 duration() const;
    State state() const;
    State previousState() const;
    quint64 elapsedTime() const;
    const std::vector<std::unique_ptr<SoundEvent>>& soundEvents() const;

    // Serializable interface
public:
    QVariant toVariant() const override;
    void fromVariant(const QVariant &variant) override;

signals:
    void stateChanged(State state);

public slots:
    void play();
    void record();
    void stop();
    void addSoundEvent(int soundEventId);

private:
    void clear();
    void setState(State state);

private:
    qint64 mDuration;
    std::vector<std::unique_ptr<SoundEvent>> mSoundEvents;
    QElapsedTimer mTimer;
    State mState;
    State mPreviousState;
};

#endif // TRACK_H
