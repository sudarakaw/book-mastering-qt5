#ifndef SOUNDEFFECTWIDGET_H
#define SOUNDEFFECTWIDGET_H

#include <QLabel>
#include <QPushButton>
#include <QSoundEffect>
#include <QWidget>

class SoundEffectWidget : public QWidget
{
    Q_OBJECT
public:
    explicit SoundEffectWidget(QWidget *parent = nullptr);

    void loadSound(const QUrl& url);

    void setId(int id);
    void setName(const QString& name);
    Qt::Key triggerKey() const;
    void setTriggerKey(const Qt::Key& triggerKey);

signals:
    void soundPlayed(int soundId);

public slots:
    void play();
    void triggerPlayButton();

    // QWidget interface
protected:
    void dragEnterEvent(QDragEnterEvent *event) override;
    void dropEvent(QDropEvent *event) override;

private:
    int mId;
    QSoundEffect mSoundEffect;
    QPushButton* mPlayButton;
    QLabel* mFilenameLabel;
    Qt::Key mTriggerKey;
};

#endif // SOUNDEFFECTWIDGET_H
